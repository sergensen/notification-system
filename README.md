# Notification System

Simple notification system implemented as testing task. 
The goal of the service is to send notifications to different channels.

# How it works

Notification microservice consumes messages from Kafka and sends notification to different channels. 
Supported channels: email, sms, slack. 

To add new channel it requires to add new value to `com.sumup.notification.domain.Channel` 
and to implement new `NotificationSender`.

To deliver a message exactly once, the application relies on Apache Kafka: 
messages are read successively for partition in topic by consumer group.
In case of horizontal scalability it requires to use the same group id among all instances and 
to have number of partitions more or equal than number of application instances.
To prevent retried sending of message, application uses idempotent check relying on user, 
time when notification was created, delivery channel, contact information and text of message.

To ensure a message wasn't lost application uses retryable topics and dead-letter topic.
Application doesn't store any state so can be horizontally scaled.

N.B. application doesn't support order of notifications in case of any problems to provide non-blocking sending.

# How to run application locally

Run infrastructure services using docker-compose:

`docker-compose up -d`

After database service is started, it requires to create database in PostgreSQL (only on first run):

`create database notification`

Then build application:

`./gradlew build`

Then start application:

`java -jar build/libs/notification-system-0.0.1-SNAPSHOT.jar`

# How to test application locally

Send message to kafka:

`echo '{"text": "Hello world", "userId": 234234, "channel": "EMAIL", "timestamp": 1653385537338, 
"contactInformation": "test_acc@gmail.com"}' | 
kafka-console-producer.sh --bootstrap-server localhost:9092 --topic notification`

# Possible improvements

- add distributed cache (e.g. Redis, to cache notifications for idempotent check)
- add contact information validation depending on delivery channel
(e.g. for SMS - validate phone, for email - email address, etc.)
- add subscription functionality: create REST API to allow users to subscribe/unsubscribe to/from different channels 
removing responsibility of choosing delivery channel from client
- add dead-letter topic management functionality to be able to analyze why message wasn't sent
- add REST API to be able to send notifications manually 
(could be used along with dead-letter topic management 
when an error in message found and need to send a message manually)