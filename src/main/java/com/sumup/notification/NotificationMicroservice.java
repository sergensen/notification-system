package com.sumup.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationMicroservice {
    public static void main(String[] args) {
        SpringApplication.run(NotificationMicroservice.class);
    }
}
