package com.sumup.notification.domain;

/**
 * Supported channels for sending notifications
 */
public enum Channel {
    EMAIL,
    SMS,
    SLACK
}
