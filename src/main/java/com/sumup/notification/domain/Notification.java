package com.sumup.notification.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table
public class Notification {
    @Id
    @SequenceGenerator(name = "notificationSeq", sequenceName = "notification_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notificationSeq")
    @Column
    private Long id;

    @Column
    private String text;

    @Column
    private Long userId;

    @Enumerated(EnumType.STRING)
    @Column
    private Channel channel;

    @Column
    private String contactInformation;

    @Column
    private Instant createdAt;

    @Column
    private Instant sentAt;

    @PrePersist
    public void prePersist() {
        this.sentAt = Instant.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant time) {
        this.createdAt = time;
    }

    public Instant getSentAt() {
        return sentAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return Objects.equals(id, that.id) && Objects.equals(text,
                that.text) && Objects.equals(userId,
                that.userId) && channel == that.channel && Objects.equals(createdAt,
                that.createdAt) && Objects.equals(sentAt, that.sentAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, userId, channel, createdAt, sentAt);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", userId=" + userId +
                ", channel=" + channel +
                ", time=" + createdAt +
                ", sentAt=" + sentAt +
                '}';
    }
}
