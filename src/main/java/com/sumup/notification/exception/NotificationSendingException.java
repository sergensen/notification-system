package com.sumup.notification.exception;

/**
 * Thrown when notification could not be sent
 */
public class NotificationSendingException extends RuntimeException {
    public NotificationSendingException() {
    }

    public NotificationSendingException(String message) {
        super(message);
    }

    public NotificationSendingException(String message, Throwable cause) {
        super(message, cause);
    }
}
