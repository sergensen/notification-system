package com.sumup.notification.exception;

import com.sumup.notification.domain.Channel;

/**
 * Thrown to indicate that application doesn't support specified channel
 */
public class UnsupportedChannelException extends RuntimeException {
    public UnsupportedChannelException(Channel channel) {
        super(String.format("Unsupported channel: %s", channel));
    }

    public UnsupportedChannelException(String message) {
        super(message);
    }
}
