package com.sumup.notification.integration.kafka;

import com.sumup.notification.integration.kafka.dto.NotificationDto;
import com.sumup.notification.service.NotificationService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.retrytopic.TopicSuffixingStrategy;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.ValidationException;

import static com.sumup.notification.integration.kafka.mapper.NotificationMapper.NOTIFICATION_MAPPER;

@Component
public class NotificationConsumer {
    private final NotificationService notificationService;

    public NotificationConsumer(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RetryableTopic(
            attempts = "4",
            backoff = @Backoff(delay = 5000, multiplier = 2.0),
            autoCreateTopics = "false",
            topicSuffixingStrategy = TopicSuffixingStrategy.SUFFIX_WITH_INDEX_VALUE,
            exclude = ValidationException.class
    )
    @KafkaListener(topics = "${notification.kafka.topics.message}")
    public void handleMessage(@Payload @Validated NotificationDto notificationDto) {
        notificationService.sendNotification(NOTIFICATION_MAPPER.notificationDtoToNotification(notificationDto));
    }
}
