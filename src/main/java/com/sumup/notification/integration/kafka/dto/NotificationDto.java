package com.sumup.notification.integration.kafka.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class NotificationDto {
    /**
     * Notification text that need to be sent to user
     */
    @NotNull
    private String text;
    /**
     * For whom need to send notification
     */
    @NotNull
    private Long userId;
    /**
     * When notification was created
     */
    @Positive
    private long timestamp;
    /**
     * To which channel notification should be sent
     * @see com.sumup.notification.domain.Channel list of supported channels
     */
    @NotNull
    private String channel;
    /**
     * Contact information for channel. It could be phone number, email or any other contact information for
     * notification
     */
    @NotNull
    private String contactInformation;
}
