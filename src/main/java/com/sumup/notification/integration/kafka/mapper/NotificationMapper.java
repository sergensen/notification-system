package com.sumup.notification.integration.kafka.mapper;

import com.sumup.notification.domain.Notification;
import com.sumup.notification.integration.kafka.dto.NotificationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface NotificationMapper {
    NotificationMapper NOTIFICATION_MAPPER = Mappers.getMapper(NotificationMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", expression = "java(java.time.Instant.ofEpochMilli(src.getTimestamp()))")
    Notification notificationDtoToNotification(NotificationDto src);
}
