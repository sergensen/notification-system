package com.sumup.notification.integration.sms;

import com.sumup.notification.domain.Channel;
import com.sumup.notification.domain.Notification;
import com.sumup.notification.service.NotificationSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SmsNotificationSender implements NotificationSender {
    private static final Logger logger = LoggerFactory.getLogger(SmsNotificationSender.class);

    @Override
    public boolean sendNotification(Notification notification) {
        // TODO: implement specific sending logic
        logger.info("Notification sent to SMS channel");
        return true;
    }

    @Override
    public Channel getNotificationChannel() {
        return Channel.SMS;
    }
}
