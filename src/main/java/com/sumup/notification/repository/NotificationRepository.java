package com.sumup.notification.repository;

import com.sumup.notification.domain.Channel;
import com.sumup.notification.domain.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, String> {
    List<Notification> findByUserIdAndCreatedAtAndChannel(Long userId, Instant createdAt, Channel channel);
}
