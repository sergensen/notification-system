package com.sumup.notification.service;

import com.sumup.notification.domain.Channel;
import com.sumup.notification.domain.Notification;

/**
 * Common interface for sending notifications. To add new implementation it requires to implement this interface
 */
public interface NotificationSender {
    /**
     * Send notification to specified channel
     * @param notification notification to send
     * @return if notification was sent
     */
    boolean sendNotification(Notification notification);

    /**
     * To which channel notification could be sent
     * @return supported channel
     */
    Channel getNotificationChannel();
}
