package com.sumup.notification.service;

import com.sumup.notification.domain.Channel;
import com.sumup.notification.domain.Notification;
import com.sumup.notification.exception.UnsupportedChannelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class NotificationSenderFacade {
    private static final Logger logger = LoggerFactory.getLogger(NotificationSenderFacade.class);

    private final Map<Channel, NotificationSender> channelNotificationSenders;

    public NotificationSenderFacade(List<NotificationSender> notificationSenders) {
        this.channelNotificationSenders = notificationSenders.stream()
                .collect(Collectors.toMap(NotificationSender::getNotificationChannel, Function.identity()));
    }

    /**
     * Responsible for sending notification to specified channel using certain {@link NotificationSender}
     *
     * @param notification notification to be sent
     * @throws com.sumup.notification.exception.UnsupportedChannelException if channel isn't supported by application
     * @return if notification was sent to specific channel
     */
    public boolean sendNotification(Notification notification) {
        NotificationSender notificationSender = channelNotificationSenders.get(notification.getChannel());
        if (notificationSender == null) {
            logger.error("Channel {} is not supported. Unable to send notification {}", notification.getChannel(),
                    notification);
            throw new UnsupportedChannelException(notification.getChannel());
        }

        return notificationSender.sendNotification(notification);
    }
}
