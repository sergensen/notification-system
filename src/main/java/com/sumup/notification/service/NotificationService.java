package com.sumup.notification.service;

import com.sumup.notification.domain.Notification;
import com.sumup.notification.exception.NotificationSendingException;
import com.sumup.notification.repository.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class NotificationService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    private final NotificationSenderFacade notificationSenderFacade;
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationSenderFacade notificationSenderFacade,
            NotificationRepository notificationRepository) {
        this.notificationSenderFacade = notificationSenderFacade;
        this.notificationRepository = notificationRepository;
    }

    /**
     * Method fulfills sending operation.
     * It supports idempotency, so repeated calls to the method don't lead to multiple sending
     *
     * @param notification notification to send
     * @throws NotificationSendingException if notification wasn't sent
     */
    public void sendNotification(Notification notification) {
        if (notificationAlreadySent(notification)) {
            logger.debug("Notification {} was already sent before", notification);
            return;
        }

        if (notificationSenderFacade.sendNotification(notification)) {
            notificationRepository.save(notification);
            logger.debug("Notification {} was successfully sent", notification);
        } else {
            logger.error("Unable to send notification {}", notification);
            throw new NotificationSendingException(
                    String.format("Unable to send notification with id %d", notification.getId()));
        }
    }

    /**
     * Checks if notification was already sent. Checks are carried out by fields: user id, channel, created time,
     * message text and contact information
     * @param notification new notification which should be tested on persisting in database
     * @return if notification was already sent
     */
    private boolean notificationAlreadySent(Notification notification) {
        return notificationRepository.findByUserIdAndCreatedAtAndChannel(
                notification.getUserId(), notification.getCreatedAt(), notification.getChannel())
                .stream()
                .anyMatch(persisted -> persisted.getText().equals(notification.getText())
                        && persisted.getContactInformation().equals(notification.getContactInformation()));
    }
}
