package com.sumup.notification.service;

import com.sumup.notification.domain.Channel;
import com.sumup.notification.domain.Notification;
import com.sumup.notification.repository.NotificationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NotificationServiceTest {
    @Mock
    private NotificationRepository notificationRepository;

    @Mock
    private NotificationSenderFacade notificationSenderFacade;

    @InjectMocks
    private NotificationService notificationService;

    @Test
    void notificationShouldBeSentIfNotExists() {
        long userId = 123L;
        Channel channel = Channel.EMAIL;
        Instant time = Instant.now();
        String text = "Hello world";
        String contactInformation = "test_acc@sumup.com";

        Notification notification = createNotification(userId, time, channel, text, contactInformation);

        when(notificationRepository.findByUserIdAndCreatedAtAndChannel(userId, time, channel))
                .thenReturn(Collections.emptyList());
        when(notificationSenderFacade.sendNotification(notification))
                .thenReturn(true);

        notificationService.sendNotification(notification);

        verify(notificationRepository).save(notification);
        verify(notificationSenderFacade).sendNotification(notification);
    }

    @Test
    void notificationShouldBeSentIfTextDiffers() {
        long userId = 123L;
        Channel channel = Channel.EMAIL;
        Instant time = Instant.now();
        String existingText = "Hello world";
        String newText = "Brand new Hello world";
        String contactInformation = "test_acc@sumup.com";

        Notification existingNotification = createNotification(userId, time, channel, existingText, contactInformation);
        Notification newNotification = createNotification(userId, time, channel, newText, contactInformation);

        when(notificationRepository.findByUserIdAndCreatedAtAndChannel(userId, time, channel))
                .thenReturn(List.of(existingNotification));
        when(notificationSenderFacade.sendNotification(newNotification))
                .thenReturn(true);

        notificationService.sendNotification(newNotification);

        verify(notificationRepository).save(newNotification);
        verify(notificationSenderFacade).sendNotification(newNotification);
    }

    @Test
    void notificationShouldBeSentIfContactInformationDiffers() {
        long userId = 123L;
        Channel channel = Channel.EMAIL;
        Instant time = Instant.now();
        String text = "Hello world";
        String existingContactInformation = "test_acc@sumup.com";
        String newContactInformation = "new_test_acc@sumup.com";

        Notification existingNotification = createNotification(userId, time, channel, text, existingContactInformation);
        Notification newNotification = createNotification(userId, time, channel, text, newContactInformation);

        when(notificationRepository.findByUserIdAndCreatedAtAndChannel(userId, time, channel))
                .thenReturn(List.of(existingNotification));
        when(notificationSenderFacade.sendNotification(newNotification))
                .thenReturn(true);

        notificationService.sendNotification(newNotification);

        verify(notificationRepository).save(newNotification);
        verify(notificationSenderFacade).sendNotification(newNotification);
    }

    @Test
    void notificationShouldNotBeSentIfAlreadyExists() {
        long userId = 123L;
        Channel channel = Channel.EMAIL;
        Instant time = Instant.now();
        String text = "Hello world";
        String contactInformation = "test_acc@sumup.com";

        Notification notification = createNotification(userId, time, channel, text, contactInformation);

        when(notificationRepository.findByUserIdAndCreatedAtAndChannel(userId, time, channel))
                .thenReturn(List.of(notification));

        notificationService.sendNotification(notification);

        verify(notificationRepository, never()).save(notification);
        verify(notificationSenderFacade, never()).sendNotification(notification);
    }

    private Notification createNotification(long userId, Instant time, Channel channel, String text,
            String contactInformation) {
        Notification notification = new Notification();
        notification.setId(5L);
        notification.setChannel(channel);
        notification.setText(text);
        notification.setContactInformation(contactInformation);
        notification.setUserId(userId);
        notification.setCreatedAt(time);
        return notification;
    }
}
